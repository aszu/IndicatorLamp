function dynamicWifiConnect()
   print('[BOOT] Connecting to wifi, scanning networks')
   wifi.sta.getap( function (names)
      ws2812.write(string.char(0,0,0,0,0,0,0,0,1))

      local gotWifi = 0
      local name
      local nameTmp
      for name in pairs(names) do
         nameTmp = name
         name = name:gsub(' ', '_')
         print('[BOOT] Network found: "'..nameTmp..'", checking existence of config file: "'..'wifiauto_'..name..'.lua'.."'.")
         local result, existence = pcall(function() return file.exists('wifiauto_'..name..'.lua') end);
         if result and existence then
            print('[BOOT] Config found for wifi network. Executing.')
            gotWifi = 1
            require('wifiauto_'..name)
            ws2812.write(string.char(0,0,0,0,0,0,0,0,0,0,0,1))

            print('[BOOT] Executed')
            break
         end
      end
      if (gotWifi == 0) then
         ws2812.write(string.char(0,0,0,0,1,0,0,0,0,0,0,0))
         print('[BOOT] Wifi not connected - no autofile for any of networks found.')
      end
   end)
end


function initAction()


   print("[BOOT]  ---- THIS IS A LUA SCRIPT ON NODEMCU                                       ----");
   print("[BOOT]  ---- On windows, you may use LuaLoader to have fun with the device.        ----");
   print("[BOOT]  ---- You may find one here: 'https://benlo.com/esp8266/'                   ----");
   print("[BOOT]  ---- Remember - suggested bitrate is 115200, no matter what is the default ----");
   print("[BOOT]  ---- the program suggests.                                                 ----");
   print("[BOOT]  ---- On Linux (and maybe also on Windows) the better solution may be       ----");
   print("[BOOT]  ---- nodemcu-tool written for nodejs (npm install -g nodemcu-tool, if my   ----");
   print("[BOOT]  ---- memory is right) and using a command line.                            ----");
   print("[BOOT]  ===============================================================================");
   print("[BOOT]  ---- To set me a value use URL:                                            ----");
   print("[BOOT]  ----      https://dweet.io/dweet/for/<device_name>?R=0&B=2&G=1             ----");

   local string, pos
   wifi.setmode(wifi.STATION)

   if (file.exists('mac.cfg')) then
      file.open('mac.cfg');
      wifi.sta.setmac(file.read(17));
      file.close()
   end

   print("[BOOT] Device MAC is "..wifi.sta.getmac())

   if (file.exists('name.cfg')) then
      file.open('name.cfg')
      string = file.read()
      pos = string.find(string, '\n')-1
      string = string.sub(string, 1, pos)
      file.close()
   else
      string = "INDICATOR_"..encoder.toHex(node.flashid())
   end

   _DEVICENAME_ = string

   print("[BOOT] Device name will be: '".._DEVICENAME_.."'.")

   -- connect to wifi, prepare to set current time on connection made.
   wifi.sta.eventMonReg(wifi.STA_GOTIP,
      function (prevState)
         rtctime = require('rtctime')
         print('[BOOT] Connected to wifi. Device IP is '..wifi.sta.getip())
         http.get('http://aszu.pl:8181/.../services/now/1000', nil,
         function(s,b)
            print('[BOOT] Request for time ended in status '..s)
            if (s == 200) then
               if tonumber(b) == nil or tonumber(b) < 10 then
                  print('[BOOT] Cannot feed RTC with "'..b..'"')
                  node.restart()
               else
                  rtctime.set(tonumber(b))
                  print('[BOOT] RTC set, '..rtctime.get())
                  pcall(function() require('updateAgent'):init() end)

                  pcall(startApplication)
               end
            else
               print ('[BOOT] RTC will not be set.')
               pcall(startApplication)

            end
         end)
      end
   )

--   wifi.sta.eventMonReg(wifi.eventmon.STA_DISCONNECTED,
--      function (T)
--         print('[BOOT] Unable to connect to AP')
--         ws2812.write(string.char(0,1,0,0,0,0,0,1,0,0,0,0))
--      end
--   )



   wifi.sta.eventMonStart()

   --wifi.sta.connect()
   pcall(dynamicWifiConnect())

end


function startApplication()
   if (file.exists('autostart.lua')) then
      print('[BOOT] autostart.lua found - executing.')
      require('autostart');
   else
      print('[BOOT] autostart.lua not found, do something manually.')
   end
end

tmr.alarm(6,5000,tmr.ALARM_SINGLE,
function ()
   print('[BOOT] Initialization...')
   ws2812.write(string.char(0,0,0,0,0,1))
   if (file.exists('preinit.lua')) then
      require('preinit')
   else
      print("[BOOT] preinit.lua not found");
   end
   pcall(initAction)
end
);


ws2812.init()
ws2812.write(string.char(0,0,1,0,0,0,0,0,0,0,0,0))