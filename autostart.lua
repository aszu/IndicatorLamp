chainSize = 4
checkInterval=5
BLINK_COUNTER = 0
BLINK_SPEED = 0

--- http://dweet.io/dweet/for/INDICATOR_31343538343135?R=255&G=0&B=0&T=1&R=255&G=0&B=0&T=0.5&R=0&G=0&B=0&T=10&R=0&G=0&B=255&T=0.5&R=0&G=0&B=255&T=1&R=0&G=0&B=0&T=0.5&R=0&G=0&B=0&T=10&R=0&G=0&B=0&T=0.5
--- http://dweet.io/dweet/for/INDICATOR_31343538343135?R=255&G=0&B=0&T=1&R=0&G=20&B=0&T=0.5


--- in lua, it is not necessary to init globals with "nil", but I'd like to
--- tell here "Well, I'll use that variables later" to the reader, not to the
--- script.
cR = nil
cG = nil
cB = nil
cT = {}

animationStep = 1
animationStepStarted = 0

ws2812.init()
bufW= ws2812.newBuffer(chainSize,3)

bufW:fill(0,0,0)
bufW:set(1,0,3,0)
bufW:set(2,3,0,0)
bufW:set(3,0,0,3)
bufW:set(4,1,1,1)
ws2812.write(bufW)



fetcher = tmr.create()

_tabInsert = table.insert
_tabGetn   = table.getn
_cjDecode = cjson.decode
_url = 'http://dweet.io/get/latest/dweet/for/'.._DEVICENAME_
_tmrInterval = fetcher.interval

fetcher:register(checkInterval*1000, tmr.ALARM_AUTO, function()
   --brake()
   http.get(_url, nil,
   function(status, data)
      __measB = tmr.now()
      if (status ~= 200) then
         print('[LAMP] - no access to data point (status: '..status..')')
      else
         print('[LAMP] - received data')
         data = _cjDecode(data)
         if (data.this == 'succeeded') then
            local datum = data.with[1].content
            cG, cR, cB = datum.G or 0, datum.R or 0, datum.B or 0
            cT = datum.T or {}
            if (type(cG) ~= 'number') then
--               local i
--               for i = 1,  _tabGetn(cG) do
--                  cG[i] = cG[i] + 0;
--                  cR[i] = cR[i] + 0;
--                  cB[i] = cB[i] + 0;
--                  cT[i] = cT[i] + 0;
--               end
               _tabInsert(cG, cG[1])
               _tabInsert(cB, cB[1])
               _tabInsert(cR, cR[1])
            end
            if (datum.INT ~= nil and datum.INT > 0 and datum.INT < 3601) then
               _tmrInterval(fetcher, datum.INT * 1000)
            end
         end
      end
      __measE = tmr.now() - __measB
   end
   )
end)
fetcher:start()


refresher = tmr.create()
refresher:register(20, tmr.ALARM_AUTO, function()

   if (type(cR) == 'number') then
      node.setcpufreq(node.CPU80MHZ)
      bufW:fill(cG, cR, cB)
   elseif (type(cR) == 'table') then
      node.setcpufreq(node.CPU160MHZ)
      fillAnimation()
   else
      -- nottin to do!
   end
   ws2812.write(bufW)
end)
refresher:start()


function fillAnimation()
   bufW:fill(0,1,0)
   local now = tmr.now()
   local timeSinceStep = now - animationStepStarted
   if (timeSinceStep < 0) then
      timeSinceStep = timeSinceStep + 2147483648
   end
   timeSinceStep = timeSinceStep / 1000000

   stepCount = table.getn(cT)
   if (animationStep > stepCount) then
      animationStep = 1
      animationStepStarted = now
      return
   end

   local stepLength = cT[animationStep] + 0

   if (timeSinceStep > stepLength) then
      if (animationStep >= stepCount) then
         animationStep = 1
      else
         animationStep = animationStep + 1
      end
      animationStepStarted = now
      timeSinceStep = 0
      stepLength = cT[animationStep]
   end

   local oldMultiplier = (stepLength - timeSinceStep) / stepLength
   local nextMultiplier = 1 - oldMultiplier

   if (oldMultiplier < 0) then
      oldMultiplier = 0
      nextMultiplier = 1
   end

   local nR
   local nG
   local nB

   --print('[LAMP] Animation step: ', animationStep, table.getn(cR), table.getn(cG), table.getn(cB), table.getn(cT) )

   nR, nG, nB =
     cR[animationStep] * oldMultiplier + cR[animationStep+1] * nextMultiplier,
     cG[animationStep] * oldMultiplier + cG[animationStep+1] * nextMultiplier,
     cB[animationStep] * oldMultiplier + cB[animationStep+1] * nextMultiplier


  bufW:fill(nG, nR, nB)

end